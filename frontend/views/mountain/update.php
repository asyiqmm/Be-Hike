<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mountain */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mountain',
]) . ' ' . $model->Id_location;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mountains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Id_location, 'url' => ['view', 'id' => $model->Id_location]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mountain-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
