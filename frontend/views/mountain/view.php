<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mountain */

$this->title = $model->Id_location;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mountains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mountain-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->Id_location], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->Id_location], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id_location',
            'Nama_Gunung',
            'Lokasi_Gunung',
            'DPL',
        ],
    ]) ?>

</div>
