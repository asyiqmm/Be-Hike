<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mountain;

/**
 * MountainSearch represents the model behind the search form about `app\models\Mountain`.
 */
class MountainSearch extends Mountain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id_location', 'DPL'], 'integer'],
            [['Nama_Gunung', 'Lokasi_Gunung'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mountain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id_location' => $this->Id_location,
            'DPL' => $this->DPL,
        ]);

        $query->andFilterWhere(['like', 'Nama_Gunung', $this->Nama_Gunung])
            ->andFilterWhere(['like', 'Lokasi_Gunung', $this->Lokasi_Gunung]);

        return $dataProvider;
    }
}
